<?php

$filepath =realpath(dirname(__FILE__));
include_once($filepath . '/../lib/Session.php');
include_once($filepath . '/../lib/Database.php');
include_once($filepath. '/../helpers/Format.php');


/**
 * Class Admin
 */

class Admin
{
    private  $db;
    private  $fm;

    public function __construct()
    {
        $this->db =new Database();
        $this->db =new Format();

    }
    public function getAdminData($data){
        $username= $this->fm->validation($data['adminUser']);
        $adminPass=$this->db->validation($data['adminPass']);
        $username =mysqli_real_escape_string($this->db->link,$username);
        $adminPass =mysqli_real_escape_string($this->db->link,md5($adminPass));
    }

}